### Invisible Controls

A research project to test methods of using a phone (or similar device) and it's touchscreen as a simple game controller, keeping a valuable property of game controllers - you don't need to see what you're doing.

### Using

Run the program on your phone (or a similar device) and using ssh (or logcat, ...) established over a USB connection, pipe the program output to the `sendkeys.sh` script running on your host machine.  
Example quick and dirty hacks:  
`ssh user@pmos "XDG_RUNTIME_DIR=/run/user/10000 stdbuf -oL invisible_controls/invisible_controls" | ./sendkeys.sh`  
`stdbuf -oL adb shell "/data/local/coreutils/stdbuf -oL logcat" | stdbuf -oL grep TAG | stdbuf -oL sed "s/[^T]*TAG     : //g" | ./sendkeys.sh`  

Latency doesn't seem to be an issue.

### Interfaces

Currently, there is only one control interface implemented, aimed primarily at top-down games: A virtual joystick of sorts, that appears under your finger. Works great for moving one direction at a time, and even for turning. Deadzone was implemented to combat "lagging" of characters when turning 180°, but it doesn't seem like an optimal solution, decreasing responsiveness and making turns less precise.  
For games requiring more than just movement keys (most of them), using second hand for those is recommended (until movement "feels right")

### Feedback?

If you can play around with this, and tweak it to see whether you can make it "feel right", that would be highly appreciated. If you want to contribute a new control paradigm, maybe targeted at different sort of games, that's fine too. And if you just have a suggestion - but really, please, try it yourself first - feel free to open an issue.
