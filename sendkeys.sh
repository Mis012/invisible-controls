#!/bin/bash

strstr() {
  [ "${1#*$2*}" = "$1" ] && return 1
  return 0
}

while IFS= read -r line; do
	tmp=$(echo -e "$line" | tr -d '[:space:]')
	strstr "$line" press && xdotool keydown "${tmp: -1}"
	strstr "$line" release && xdotool keyup "${tmp: -1}"
done
