#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <GLES2/gl2.h>
#include <SDL2/SDL.h>

#ifdef __ANDROID__
#include "SDL.h"
#include <android/log.h>
#undef printf
#define printf(...) __android_log_print(ANDROID_LOG_DEBUG, "TAG", __VA_ARGS__);
#endif


#define GLFW_INCLUDE_ES2
#define NANOVG_GLES2_IMPLEMENTATION
#include "nanovg.h"
#include "nanovg_gl.h"
#include "nanovg_gl_utils.h"

int main(int argc, char **argv) {
	SDL_Window *window = NULL;
	SDL_GLContext context;
	NVGcontext* vg = NULL;

	int flags = SDL_INIT_EVERYTHING
	if (SDL_Init(flags) < 0)//if it doesn't work, try and log a helpful error
	{
		printf("error: sdl2 init failed: %s", SDL_GetError());
		return 1;
	}
	//better safe than sorry vv
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 8);
	window = SDL_CreateWindow("Example", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1024, 800,
		SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_SHOWN | SDL_WINDOW_ALLOW_HIGHDPI);//only SDL_WINDOW_OPENGL may suffice
	if (!window)//if it doesn't work, lower the bar
	{
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 0);
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 0);
		window = SDL_CreateWindow("Example", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1024, 800,
			SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_SHOWN | SDL_WINDOW_ALLOW_HIGHDPI);//same as above
		if (!window)//if this doesn't work, it's probably becouse of an error. so we log it
		{
			printf("error2: sdl2 init failed: %s", SDL_GetError());
			return 1;
		}
	}
	context = SDL_GL_CreateContext(window);
	SDL_GL_MakeCurrent(window, context);

	// pretty much just used for nvgRadToDeg, should probably be removed if we don't need to draw stuff
	vg = nvgCreateGLES2(NVG_ANTIALIAS | NVG_STENCIL_STROKES | NVG_DEBUG);//initialize nanovg. if it fails, log it. there is no get_error though.
	if (vg == NULL) {
		printf("error: nanovg init failed");
		return 1;
	}

	SDL_GL_SwapWindow(window); //phosh won't give us a window if we don't appear to be drawing in it

	int winWidth;
	int winHeight;
	SDL_GetWindowSize(window, &winWidth, &winHeight);

	int touching = 0;
	int center_x;
	int center_y;

	char direction = 0;
	char prev_direction = 0;

	int quit=0;
	while (!quit) {
		SDL_Event event;

		while(SDL_PollEvent(&event)) {
			switch(event.type) {
				case SDL_QUIT:
					quit=1;
					break;
				case SDL_MOUSEBUTTONDOWN:
					center_x = event.button.x;
					center_y = event.button.y;

					touching = 1;

					break;
				case SDL_MOUSEBUTTONUP:
					if(direction)
						printf("release: %c\n", prev_direction);

					touching = 0;
					prev_direction = 0;

					printf("---\n");
					break;
				case SDL_MOUSEMOTION:
					if (touching) {
						int vec_x = event.motion.x - center_x;
						int vec_y = event.motion.y - center_y;

						double angle = atan2(vec_y, vec_x);
						double len = sqrt(pow(vec_x, 2) + pow(vec_y, 2));
						float deg = nvgRadToDeg((float)angle);
						if (deg > -135 && deg < -45)
							direction = 'w';
						else if (deg > -45 && deg < 45)
							direction = 'd';
						else if (deg > 45 && deg < 135)
							direction = 's';
						else
							direction = 'a';

						if(direction != prev_direction) {
							if(prev_direction)
								printf("release: %c\n", prev_direction);
							printf("press: %c\n", direction);

							prev_direction = direction;
						}

						if(len > 40 /*FIXME ARBITRARY*/) {
							center_x = event.button.x;
							center_y = event.button.y;
						}
					}

					break;
			}
		}
	}

	return 0;
}
